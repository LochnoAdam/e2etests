/// <reference types="cypress" />
import HomePage from '../../pages/homePage'
import ShoppingCartPage from '../../pages/shoppingCartPage'
import AddressesPage from '../../pages/addressesPage'
import ShippingPage from '../../pages/shippingPage'
import PaymentPage from '../../pages/paymentPage'
import WishListPage from '../../pages/wishListPage'

describe('My First Test', () => {
    let homePage = new HomePage();
    let shoppingCartPage = new ShoppingCartPage();
    let addressesPage = new AddressesPage();
    let shippingPage = new ShippingPage();
    let paymentPage = new PaymentPage();
    let wishListPage = new WishListPage();

    before(() => {
        homePage.registerUser()
    })

    beforeEach(() => {
        cy.clearCookies()
        cy.clearLocalStorage()
        homePage.loginUser()
    })

    it('add multiplet items and decrease amount', () => {
        homePage.open();
        homePage.openSummerDresses()
        homePage.openSingleSummerDress()
        homePage.addDressesToCart()
        homePage.goToCart()
        shoppingCartPage.assertAmount()
        shoppingCartPage.decreaseAmount()
        shoppingCartPage.assertDecreasedAmount()
    })

    it('checks if user can buy item', () => {
        homePage.open();
        homePage.addItemToCart();
        homePage.proceedToCheckOut();
        shoppingCartPage.proceed()
        addressesPage.proceed()
        shippingPage.proceed()
        paymentPage.choosePayByBankWire()
        paymentPage.confirmMyOrder()
        paymentPage.assertOrderWasMade()
    })

    it('add sale dress to cart', () => {
        homePage.open();
        homePage.pickDiscountedItem()
        homePage.goToWishList()
        wishListPage.choosePayByBankWire()
    })
})
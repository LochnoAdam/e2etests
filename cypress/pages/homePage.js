/// <reference types="cypress" />
import AuthenticationPage from '../pages/authenticationPage'
import RegisterPage from '../pages/registerPage'

let authenticationPage = new AuthenticationPage();
let registerPage = new RegisterPage();

class HomePage {
    constructor() {
        this.elements = {
            signInButton: 'a[class="login"]',
            singleItem: 'a[class="product-name"]',
            addToCartButton: 'div a[title="Add to cart"] span',
            proceedToCheckOutButton: 'div[class="button-container"] a[title="Proceed to checkout"] span',
            navBarWomen: '#block_top_menu ul li a[title="Women"]',
            eveningDresses: 'ul li a[title="Summer Dresses"]',
            summerDressImg: 'div[class="left-block"]',
            openMoreButton: 'a[title="View"] span',
            sizeSelect: 'select[id="group_1"]',
            quantityInput: 'input[id="quantity_wanted"]',
            addDressToCartButton: 'button[name="Submit"]',
            closeProductAddedToCartModal: 'span[title="Close window"]',
            goToCartButton: 'a[title="View my shopping cart"]',
            discountMark: 'span[class="price-percent-reduction"]',
            wishListButton: 'a[id="wishlist_button"]',
            addedToWishListPopUp: 'p[class="fancybox-error"]',
            closeAddedToWishListPopUpButton: 'a[title="Close"]',
            myAccount: 'a[title="View my customer account"]',
            goToMyWishlist: 'a[title="My wishlists"]'
        }

        this.testData = {
            productName: 'Faded Short Sleeve T-shirts'
        }

        this.yellowDress = {
            amount: '2',
            colour: 'Yellow',
            size: 'S'
        }

        this.orangeDress = {
            amount: '3',
            colour: 'Orange',
            size: 'M'
        }

        this.blueDress = {
            amount: '7',
            colour: 'Blue',
            size: 'L'
        }
    };
    open() {
        cy.visit('http://automationpractice.com/index.php')
    }

    goToSignIn() {
        cy.get(this.elements.signInButton).click();
    }

    registerUser() {
        this.open();
        this.goToSignIn();
        authenticationPage.provideEmail()
        registerPage.provideUserData()
        registerPage.registerUSer()
    }

    loginUser() {
        this.open();
        this.goToSignIn();
        authenticationPage.provideCredentials();
    }

    addItemToCart() {
        cy.get(this.elements.singleItem).contains(this.testData.productName).trigger('mouseover')
        cy.get(this.elements.addToCartButton).first().should('be.visible').click()
    }

    proceedToCheckOut() {
        cy.get(this.elements.proceedToCheckOutButton).should('be.visible')
        cy.get(this.elements.proceedToCheckOutButton).click()
    }

    openSummerDresses() {
        cy.get(this.elements.navBarWomen).should('be.visible')
        cy.get(this.elements.navBarWomen).siblings('ul').invoke('attr', 'style', 'display:block')
        cy.get(this.elements.eveningDresses).should('be.visible')
        cy.get(this.elements.eveningDresses).contains('Summer Dresses').click();
    }

    openSingleSummerDress() {
        cy.get(this.elements.summerDressImg).eq(0).should('be.visible')
        cy.get(this.elements.summerDressImg).eq(0).trigger('mouseover')
        cy.get(this.elements.openMoreButton).eq(0).should('be.visible')
        cy.get(this.elements.openMoreButton).eq(0).click()
    }

    addDressesToCart() {
        this.pickDress(this.yellowDress.colour, this.yellowDress.size, this.yellowDress.amount)
        this.pickDress(this.blueDress.colour, this.blueDress.size, this.blueDress.amount)
        this.pickDress(this.orangeDress.colour, this.orangeDress.size, this.orangeDress.amount)
    }

    selectDressColour(colour) {
        cy.get(`ul[id="color_to_pick_list"] li a[name="${colour}"]`).click()
    }

    pickDress(colour, size, amount) {
        this.selectDressColour(colour)
        cy.get(this.elements.sizeSelect).select(size)
        cy.get(this.elements.quantityInput).clear()
        cy.get(this.elements.quantityInput).type(amount)
        cy.get(this.elements.addDressToCartButton).click()
        cy.get(this.elements.closeProductAddedToCartModal).should('be.visible')
        cy.get(this.elements.closeProductAddedToCartModal).click()
    }

    goToCart() {
        cy.get(this.elements.goToCartButton).should('be.visible').click();
    }

    pickDiscountedItem() {
        cy.get(this.elements.discountMark).contains('%').parents('li').click()
    }

    addItemToWishlist() {
        cy.get(this.elements.wishListButton).should('be.visible').click()
        cy.get(this.elements.addedToWishListPopUp).should('be.visible')
        cy.get(this.elements.closeAddedToWishListPopUpButton).click()
    }
    goToWishList() {
        cy.get(this.elements.myAccount).click()
        cy.get(this.elements.goToMyWishlist).should('be.visible').click()
    }
}

export default HomePage;
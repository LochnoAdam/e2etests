/// <reference types="cypress" />

class WishListPage {
    constructor() {
        this.elements = {
            firstWishListButton: 'tr td a'
        }
        this.testData = {}
    };

    choosePayByBankWire() {
        cy.get(this.elements.firstWishListButton).contains('My wishlist').should('be.visible')
    }

}
export default WishListPage;
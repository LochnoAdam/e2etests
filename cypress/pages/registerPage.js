/// <reference types="cypress" />
const faker = require('faker');

class registerPage {
    constructor() {
        this.elements = {
            genderRadio: 'input[id="id_gender1"]',
            firstNameInput: 'input[id="customer_firstname"]',
            lastNameInput: 'input[id="customer_lastname"]',
            password: 'input[id="passwd"]',
            dayOfBirth: "select[id='days']",
            monthOfBirth: "select[id='months']",
            yearOfBirth: "select[id='years']",
            addressFirstNameInput: 'input[id="firstname"]',
            addressLastNameInput: 'input[id="lastname"]',
            addressInput: 'input[id="address1"]',
            cityInput: 'input[id="city"]',
            stateSelect: 'select[id="id_state"]',
            zipCodeInput: 'input[id="postcode"]',
            mobilePhoneInput: 'input[id="phone_mobile"]',
            registerButton: 'button[id="submitAccount"]'
        }

        this.testData = {
            firstName: `${faker.name.firstName()}`,
            lastName: `${faker.name.lastName()}`,
            dayOfBirth: '24',
            monthOfBirth: '3',
            yearOfBirth: '1994',
            address: '44-100',
            city: 'Gliwice',
            state: '2',
            zipCode: '99723',
            mobilePhone: '0123456789',
        }
    };

    provideUserData() {
        cy.get(this.elements.genderRadio).click();
        cy.get(this.elements.firstNameInput).type(this.testData.firstName)
        cy.get(this.elements.lastNameInput).type(this.testData.lastName)
        cy.readFile('cypress/fixtures/data.json').then((user) => {
            cy.get(this.elements.password).type(user.password)
        })
        cy.get(this.elements.dayOfBirth).select(this.testData.dayOfBirth)
        cy.get(this.elements.dayOfBirth).select(this.testData.dayOfBirth)
        cy.get(this.elements.monthOfBirth).select(this.testData.monthOfBirth)
        cy.get(this.elements.yearOfBirth).select(this.testData.yearOfBirth)
        cy.get(this.elements.addressInput).type(this.testData.address)
        cy.get(this.elements.cityInput).type(this.testData.city)
        cy.get(this.elements.stateSelect).select(this.testData.state)
        cy.get(this.elements.zipCodeInput).type(this.testData.zipCode)
        cy.get(this.elements.mobilePhoneInput).type(this.testData.mobilePhone)
    }

    registerUSer() {
        cy.get(this.elements.registerButton).click()
    }
}
export default registerPage;
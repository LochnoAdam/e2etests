/// <reference types="cypress" />

class ShippingPage {
    constructor() {
        this.elements = {
            proceedToCheckoutButton: 'button[type="submit"] span i',
            termsOfServiceButton: 'span input[type="checkbox"]'
        }
        this.testData = {

        }
    };

    proceed() {
        cy.get(this.elements.proceedToCheckoutButton).should('be.visible')
        cy.get(this.elements.termsOfServiceButton).click()
        cy.get(this.elements.proceedToCheckoutButton).click()
    }

}
export default ShippingPage;
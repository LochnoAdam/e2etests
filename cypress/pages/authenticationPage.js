/// <reference types="cypress" />
class AuthenticationPage {
    constructor() {
        this.elements = {
            newEmailInput: "input[id='email_create']",
            createAnAccountButton: "button[id='SubmitCreate']",
            existingEmailAddressInput: "input[id='email']",
            existingPasswordInput: "input[id='passwd']",
            signInButton: "button[id='SubmitLogin']"
        }
        this.testData = {
            emailDataFirstPart: "adamlochno+",
            emailDataSecondPart: "@gmail.com"
        }
    };

    provideEmail() {
        this.createEmail()
        cy.readFile('cypress/fixtures/data.json').then((user) => {
            cy.get(this.elements.newEmailInput).type(user.email)
        })
        cy.get(this.elements.createAnAccountButton).click()
    }

    createEmail() {
        let email = this.testData.emailDataFirstPart + `${Date.now()}` + this.testData.emailDataSecondPart
        cy.writeFile('cypress/fixtures/data.json', {
            email: `${email}`,
            password: 'qwerty123456'
        })
    }

    provideCredentials() {
        cy.readFile('cypress/fixtures/data.json').then((user) => {
            cy.get(this.elements.existingEmailAddressInput).type(user.email)
            cy.get(this.elements.existingPasswordInput).type(user.password)
        })
        cy.get(this.elements.signInButton).click()
    }
}
export default AuthenticationPage;
/// <reference types="cypress" />
import HomePage from '../pages/homePage'

let homePage = new HomePage();

class ShoppingCartPage {
    constructor() {
        this.elements = {
            proceedToCheckoutButton: 'p[class*="cart_navigation"] a[title="Proceed to checkout"]',
            singleElementDescription: 'td[class="cart_description"] small a',
            dressAmount: 'input[size="2"]',
            decreaseDressAmountButtons: 'span i[class="icon-minus"]'
        }
        this.testData = {
        }
    };

    proceed() {
        cy.get(this.elements.proceedToCheckoutButton).should('be.visible')
        cy.get(this.elements.proceedToCheckoutButton).click()
    }

    assertAmount() {
        if (cy.get(this.elements.singleElementDescription)
            .eq(0)
            .contains(homePage.yellowDress.colour) &&
            cy.get(this.elements.singleElementDescription)
            .eq(0)
            .contains(homePage.yellowDress.size)) {
            cy.get(this.elements.dressAmount)
                .eq(0)
                .should('have.attr', 'value', homePage.yellowDress.amount);
        }
        if (cy.get(this.elements.singleElementDescription)
            .eq(1)
            .contains(homePage.blueDress.colour) &&
            cy.get(this.elements.singleElementDescription)
            .eq(1)
            .contains(homePage.blueDress.size)) {
            cy.get(this.elements.dressAmount)
                .eq(1)
                .should('have.attr', 'value', homePage.blueDress.amount);
        }
        if (cy.get(this.elements.singleElementDescription)
            .eq(2)
            .contains(homePage.orangeDress.colour) &&
            cy.get(this.elements.singleElementDescription)
            .eq(2)
            .contains(homePage.orangeDress.size)) {
            cy.get(this.elements.dressAmount)
                .eq(2)
                .should('have.attr', 'value', homePage.orangeDress.amount);
        }
    }

    decreaseAmount() {
        cy.get(this.elements.decreaseDressAmountButtons).each((element) => {
            cy.get(element).click()
        })
    }
    assertDecreasedAmount() {
        cy.reload()
        if (cy.get(this.elements.singleElementDescription)
            .eq(0)
            .contains(homePage.yellowDress.colour) &&
            cy.get(this.elements.singleElementDescription)
            .eq(0)
            .contains(homePage.yellowDress.size)) {
            cy.get(this.elements.dressAmount)
                .eq(0)
                .should('have.attr', 'value', homePage.yellowDress.amount - 1);
        }

        if (cy.get(this.elements.singleElementDescription)
            .eq(1)
            .contains(homePage.orangeDress.colour) &&
            cy.get(this.elements.singleElementDescription)
            .eq(1)
            .contains(homePage.orangeDress.size)) {
            cy.get(this.elements.dressAmount)
                .eq(1)
                .should('have.attr', 'value', homePage.orangeDress.amount - 1);
        }

        if (cy.get(this.elements.singleElementDescription)
            .eq(2)
            .contains(homePage.blueDress.colour) &&
            cy.get(this.elements.singleElementDescription)
            .eq(2)
            .contains(homePage.blueDress.size)) {
            cy.get(this.elements.dressAmount)
                .eq(2)
                .should('have.attr', 'value', homePage.blueDress.amount - 1);
        }
    }
}

export default ShoppingCartPage;
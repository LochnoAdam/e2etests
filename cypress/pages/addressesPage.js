/// <reference types="cypress" />

class AddressesPage {
    constructor() {
        this.elements = {
            proceedToCheckoutButton: 'button[type="submit"] span i'
        }
        this.testData = {

        }
    };

    proceed() {
        cy.get(this.elements.proceedToCheckoutButton).should('be.visible')
        cy.get(this.elements.proceedToCheckoutButton).click()
    }
}
export default AddressesPage;
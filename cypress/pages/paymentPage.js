/// <reference types="cypress" />

class PaymentPage {
    constructor() {
        this.elements = {
            payByBankWireButton: "a[class='bankwire']",
            confirmMyOrderButton: "button[type='submit'] span i",
            orderCompleteDiv: "p[class='cheque-indent'] strong[class='dark']"
        }
        this.testData = {
            orderCompleteData: "Your order on My Store is complete."
        }
    };

    choosePayByBankWire() {
        cy.get(this.elements.payByBankWireButton).should('be.visible')
        cy.get(this.elements.payByBankWireButton).click()
    }
    confirmMyOrder() {
        cy.get(this.elements.confirmMyOrderButton).should('be.visible')
        cy.get(this.elements.confirmMyOrderButton).click()
    }

    assertOrderWasMade() {
        cy.get(this.elements.orderCompleteDiv).should('be.visible')
        cy.log(cy.get(this.elements.orderCompleteDiv))
        cy.get(this.elements.orderCompleteDiv).should('contain', this.testData.orderCompleteData)
    }

}
export default PaymentPage;